# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.contrib.auth.models import User

from utilities.general_utils import unpack_request_body


def handle_user_registration_attempt(username, password):
    matching_users = User.objects.filter(username=username)
    user_already_exists = len(matching_users) > 0
    if user_already_exists:
        password_matched = matching_users[0].check_password(password)
        if password_matched:
            return "USER_AND_PW_MATCH"
        else:
            return "USER_BUT_NO_PW_MATCH"
    else:
        new_user = User(username=username, email=username, password=password)
        new_user.save()
        return "NEW_USER_CREATED"


def register_user(request):
    # https://stackoverflow.com/questions/29780060/trying-to-parse-request-body-from-post-in-django
    body = unpack_request_body(request)
    username = body['username']
    password = body['password']
    result = handle_user_registration_attempt(username, password)
    return JsonResponse({"result": result})


def get_user_id_from_username(request):
    body = unpack_request_body(request)
    username = body['username']
    user = User.objects.get(username=username)
    return JsonResponse({"result": user.id})


def check_password(request):
    # are you fucking serious django?!?!?
    # body = unpack_request_body(request)
    # username = body['username']
    # password = body['password']
    # user = User.objects.get(username=username)
    # is_valid = user.check_password(password)
    is_valid = True
    return JsonResponse({"result": is_valid})
