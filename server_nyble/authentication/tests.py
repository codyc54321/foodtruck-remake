# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.contrib.auth.models import User

from .utils import check_user_login_attempt


class AuthenticationTests(TestCase):

    @classmethod
    def setUpClass(cls):
        super(AuthenticationTests, cls).setUpClass()
        user = User(username='thisuser', password='mypassword')
        user.save()

    def test_check_user_login_attempt_correct_login(self):
        result = check_user_login_attempt(username='thisuser', password='mypassword')
        self.assertEqual(result, {'result': 'match'})

    def test_check_user_login_attempt_wrong_password(self):
        result = check_user_login_attempt(username='thisuser', password='asdfasdf')
        self.assertEqual(result, {'result': 'wrong_password'})

    def test_check_user_login_attempt_no_account(self):
        result = check_user_login_attempt(username='asfdasfdsafd', password='mypassword')
        self.assertEqual(result, {'result': 'no_account'})
