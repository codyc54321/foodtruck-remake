from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt

from rest_framework import routers

from . import views

urlpatterns = [
    # url(r'^loginCheck/', csrf_exempt(views.login_check), name='login-check'),
    url(r'^api/register-user/', csrf_exempt(views.register_user), name='register-user'),
    url(r'^api/user-id/', csrf_exempt(views.get_user_id_from_username), name='user-id'),
    url(r'^api/authenticate/', csrf_exempt(views.check_password), name='authenticate'),
]
