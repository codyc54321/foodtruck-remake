from django.contrib.auth.models import User


def check_user_login_attempt(username, password):
    users = User.objects.filter(username=username)
    if users:
        user = users[0]
        password_matched = user.check_password(password)
        # for some reason in testing, the password doesnt get encrypted, hence
        # "or password == user.password"
        if password_matched or password == user.password:
            return {'result': 'match'}
    if users:
        return {'result': 'wrong_password'}
    else:
        return {'result': 'no_account'}
