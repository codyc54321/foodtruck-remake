import json

def unpack_request_body(request):
    body_unicode = request.body.decode('utf-8')
    return json.loads(body_unicode)
