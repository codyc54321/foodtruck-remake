# import requests
import geocoder

""" this is how to do it if the geocoder package breaks again """
# def geocode_address(address):
#     url = 'https://maps.googleapis.com/maps/api/geocode/json'
#     params = {'sensor': 'false', 'address': address}
#     r = requests.get(url, params=params)
#     try:
#         results = r.json()['results']
#         location = results[0]['geometry']['location']
#         return {'lat': location['lat'], 'lng': location['lng']}
#     except:
#         return None

def geocode_address(address):
    geo_object = geocoder.google(address)
    coordinates = geo_object.latlng
    return {'lat': coordinates[0], 'lng': coordinates[1]}
