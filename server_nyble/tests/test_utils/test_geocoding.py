
from django.test import TestCase

from utils.geocoding import geocode_address


class GeocodingTests(TestCase):

    def test_geocode_address(self):
        house_location =  {'lat': 30.2314147, 'lng': -97.5930796}

        location = geocode_address("3203 Barksdale Dr, Austin TX 78725")
        self.assertEqual(location, house_location)

        location = geocode_address("3203 Barksdale Dr, Austin, TX, 78725")
        self.assertEqual(location, house_location)
