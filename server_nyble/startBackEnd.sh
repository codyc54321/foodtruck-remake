
# --------------------------------------------
# make migrations
~/venv/bin/python ./manage.py makemigrations

# --------------------------------------------
# run migrations
~/venv/bin/python ./manage.py migrate

# --------------------------------------------
# check that it works and start server
~/venv/bin/python ./manage.py runserver
