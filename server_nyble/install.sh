#!/bin/sh
# Nyble installer script
# by warezit on github: https://github.com/warezit

tput setaf 2; echo "===================================="
tput setaf 2; echo "Hello! Welcome to the Nyble installer script!"
tput setaf 2; echo "This is a setup script for the repo here: https://bitbucket.org/codyc54321/foodtruck-remake/overview"
tput setaf 2; echo "===================================="
tput sgr0;

# --------------------------------------------
# Check if run as sudo, exit if not
if [ "$(id -u)" != "0" ]; then
    tput setaf 1; echo "*** Error - Run script with 'sudo' ***"
    tput sgr0;
    exit 1
fi

# --------------------------------------------
# Ask user for sudo password
read -sp "Enter Password for [sudo]: " sudoPW

# --------------------------------------------
# Run dependancy setup process
read -p "Install dependencies for server (y/n)?" CONT
if [ "$CONT" = "y" ];
then
  # --------------------------------------------
  # Check if python3-pip is installed, and if not, then install it
  echo "Checking if python3-pip is installed..."
  (dpkg-query -W -f='${Status}' python3-pip 2>/dev/null | grep -c "python3-pip installed")
  if [ $(dpkg-query -W -f='${Status}' python3-pip 2>/dev/null | grep -c "python3-pip installed") -eq 0 ];
  then
    # install python3-pip
    echo $sudoPW | sudo -S apt-get -y install python3-pip;
  fi

  # --------------------------------------------
  # Upgrade pip
  # echo $sudoPW | sudo -S pip3 install --upgrade pip
  pip3 install --upgrade pip

  # --------------------------------------------
  # Install the rest of required software
  # https://github.com/PyMySQL/mysqlclient-python
  echo $sudoPW | sudo -S apt-get -y install libmysqlclient-dev python-mysqldb python3-dev python-dev virtualenv mysql-client-core-5.7 mysql-server screen
  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  echo $sudoPW | sudo -S apt-get -y install nodejs
  echo $sudoPW | sudo -S apt-get -y install npm
  npm install --save-dev webpack

  # Echo completed message
  tput setaf 2; echo "Dependencies installed";
  tput sgr0;
else
  tput setaf 1; echo "Dependencies install not run";
  tput sgr0;
fi

# --------------------------------------------
# make venv
read -p "Setup virtual environment (y/n)?" CONT
if [ "$CONT" = "y" ];
then
  # NEVER run virtualenv as sudo (like line below)
  # echo $sudoPW | sudo -S virtualenv ~/venv
  virtualenv ~/venv

  # --------------------------------------------
  # pip install into venv
  echo $sudoPW | sudo -S ~/venv/bin/pip install -r ./requirements.txt

  # Echo completed message
  tput setaf 2; echo "Done setting up virtualenv";
  tput sgr0;
else
  tput setaf 1; echo "virtualenv not run";
  tput sgr0;
fi

# --------------------------------------------
# create db, create user, and grant privileges
read -p "Run the MySQL setup process (y/n)?" CONT
if [ "$CONT" = "y" ];
then
  tput setaf 1; echo "*** Enter mysql password for root three times ***"
  tput setaf 2; echo "First time: Creating Database 'nybledb'... enter the mysql password for root you set during installation"
  tput sgr0;
  echo $sudoPW | sudo -S mysql -uroot -p -e "CREATE DATABASE nybledb;"
  # echo $sudoPW | sudo -S mysql -uroot -p$sudoPW -e "CREATE USER 'nyble'@'localhost' IDENTIFIED BY 'eatit2017';"
  # echo $sudoPW | sudo -S mysql -uroot -p$sudoPW -e "GRANT ALL PRIVILEGES ON nybledb.* TO 'nyble'@'localhost' IDENTIFIED BY 'eatit2017';"
  tput setaf 2; echo "Second time: Creating user 'nyble' and adding privileges on 'nybledb' ... enter the mysql password for root you set during installation"
  tput sgr0;
  echo $sudoPW | sudo -S mysql -uroot -p -e "GRANT ALL PRIVILEGES ON nybledb.* TO nyble@localhost IDENTIFIED BY 'eatit2017' WITH GRANT OPTION;"
  tput setaf 2; echo "Third time: Flushing privileges ... enter the mysql password for root you set during installation"
  tput sgr0;
  echo $sudoPW | sudo -S mysql -uroot -p -e "FLUSH PRIVILEGES;"
  tput sgr0;
  tput setaf 2; echo "MySQL setup process complete";
  tput sgr0;
else
  tput setaf 1; echo "MySQL setup process not run";
  tput sgr0;
fi

# --------------------------------------------
# start Atom install process
read -p "Do you want to install Atom Text Editor (y/n)?" CONT
if [ "$CONT" = "y" ];
then
  echo $sudoPW | sudo add-apt-repository ppa:webupd8team/atom
  echo $sudoPW | sudo apt-get -y install atom
  echo $sudoPW | sudo apm install emmet pigments minimap autocomplete-paths linter-ui-default linter git-time-machine highlight-selected minimap-highlight-selected atom-beautify intentions busy-signal jshint
  echo $sudoPW | sudo apm disable wrap-guide
  tput setaf 2; echo "Atom installed WITH extra packages! :)";
  tput sgr0;
else
  tput setaf 1; echo "Atom install not run";
  tput sgr0;
fi

# --------------------------------------------
# start server BACKend as detached screen

# kill any running screens before launching new ones
echo $sudoPW | sudo killall screen

read -p "Start server backend (y/n)?" CONT
if [ "$CONT" = "y" ];
then
  screen -dmS NybleBackEnd sh -c './startBackEnd.sh; exec bash'
  tput setaf 2; echo "Backend running on detached screen. To resume, use $(tput setab 7)sudo screen -r NybleBackEnd$(tput sgr 0)";
  tput setaf 2; echo "The backend is available at $(tput setab 7)http://127.0.0.1:8000$(tput sgr 0)"
  tput sgr0;
else
  tput setaf 1; echo "Backend not run";
  tput sgr0;
fi

# --------------------------------------------
# start server FRONTend as detached screen
read -p "Start server frontend (y/n)?" CONT
if [ "$CONT" = "y" ];
then
  screen -dmS NybleFrontEnd sh -c './startFrontEnd.sh; exec bash'
  tput setaf 2; echo "Frontend running on detached screen. To resume, use $(tput setab 7)sudo screen -r NybleFrontEnd$(tput sgr 0)";
  tput sgr0;
  # --------------------------------------------
  # complete script and list URLs
  tput setaf 2; echo "===================================="
  tput setaf 2; echo "Nyble is now fully setup & running!! Congrats!!"
  # tput setaf 2; echo "The backend is available at $(tput setab 7)http://127.0.0.1:8000$(tput sgr 0)"
  tput setaf 2; echo "The frontend is available at $(tput setab 7)http://127.0.0.1:8080$(tput sgr 0)"
  tput setaf 1; echo "To stop the backend AND frontend ... use $(tput setab 7)sudo killall screen$(tput sgr 0)"
  tput setaf 1; echo "$(tput setab 3)$(tput bold)If the frontend doesn't load right away, just give it a few seconds...$(tput sgr 0)"
  tput setaf 2; echo "===================================="
  tput sgr0;
  # --------------------------------------------
  # echo list of running screen sessions
  sudo screen -ls
  tput setaf 2; echo "===================================="
  tput sgr0;
else
  tput setaf 1; echo "Frontend not run";
  tput sgr0;
fi

# --------------------------------------------
# option to launch Atom
read -p "Open Atom (y/n)?" CONT
if [ "$CONT" = "y" ];
then
  atom ..
  tput setaf 2; echo "Atom launched";
  tput sgr0;
else
  tput setaf 1; echo "Atom not launched";
  tput sgr0;
fi
