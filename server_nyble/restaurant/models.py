# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from decimal import Decimal

from django.db import models
from django.contrib.auth.models import User

from utilities.geocoding_utils import geocode_address


class Restaurant(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False)
    phone = models.CharField(max_length=12)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, related_name="restaurants", null=True, blank=True)

    address1 = models.CharField(max_length=250, null=False, blank=False)
    address2 = models.CharField(max_length=250, null=True, blank=True)
    city = models.CharField(max_length=250, null=False, blank=False)
    state_code = models.CharField(max_length=2, null=False, blank=False)
    zip_code = models.CharField(max_length=5, null=False, blank=False)

    lat = models.DecimalField(max_digits=10, decimal_places=7, null=True, blank=True)
    lng = models.DecimalField(max_digits=10, decimal_places=7, null=True, blank=True)

    def __str__(self):
        # doesnt work with DecimalField
        # return self.name + ' lat: ' + self.lat + ' lng: ' + self.lng
        return self.name

    def save(self, *args, **kwargs):
        if not self.lat and not self.lng:
            try:
                lat_lng = self.geocode_own_address()
                self.lat = lat_lng['lat']
                self.lng = lat_lng['lng']
            except Exception as e:
                print(e)
                raise Exception("Failed to decode an address for {}".format(self.name))
        super(Restaurant, self).save(*args, **kwargs)

    def regeocode_own_address(self):
        lat_lng = self.geocode_own_address()
        self.lat = Decimal(lat_lng['lat'])
        self.lng = Decimal(lat_lng['lng'])
        self.save()

    def geocode_own_address(self):
        lat_lng = geocode_address(self.full_address)
        return lat_lng

    @property
    def full_address(self):
        full_address = self.address1 + ', '
        if self.address2:
            full_address += self.address2 + ', '
        full_address += self.city + ' ' + self.state_code + ' ' + self.zip_code
        return full_address


class MenuItem(models.Model):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, related_name="menu_items", null=False, blank=False)
    name = models.CharField(max_length=250, null=False, blank=False)
    price = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
