from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'restaurants', views.RestaurantViewSet, base_name='restaurant')
router.register(r'menus', views.RestaurantMenuViewSet, base_name='menus')


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api/restaurants-owned', csrf_exempt(views.restaurants_owned_by_this_username), name='restaurants-owned'),
    url(r'^api/update-menu', csrf_exempt(views.update_menu), name='update-menu'),
]
