from .models import Restaurant, MenuItem


def update_restaurant_menu(new_menu, restaurant_id):
    """ takes new_menu input like
        [
            {id: 12, name: 'chicken taco', price: 1.99}, # existing item
            {name: 'chicken taco', price: 1.99}, # new item because it didn't have an id before
        ]
    """
    the_restaurant = Restaurant.objects.get(pk=restaurant_id)
    old_menu = the_restaurant.menu_items.all()
    # first, delete old items that are no longer included in this new menu (if any)
    incoming_ids = set([item.get('id', None) for item in new_menu])
    for item in old_menu:
        if not item.id in incoming_ids:
            MenuItem.objects.get(id=item.id).delete()

    for item in new_menu:
        # update items
        if 'id' in item.keys():
            # https://www.smallsurething.com/list-dict-and-set-comprehensions-by-example/
            update_fields = {key: value for key, value in item.items() if key != 'id'}
            print(item)
            print(update_fields)
            # https://stackoverflow.com/questions/2712682/django-update-object
            queryset_for_existing_item = MenuItem.objects.filter(pk=item['id'])
            if not queryset_for_existing_item:
                raise Exception("MenuItem %s does not exist...can't update an item that doesn't exist" % item['id'])
            queryset_for_existing_item.update(**update_fields)
        # add new items
        else:
            item['restaurant'] = the_restaurant
            MenuItem.objects.create(**item)
