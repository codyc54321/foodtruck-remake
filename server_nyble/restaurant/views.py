import json
import time

from django.http import JsonResponse
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from rest_framework import serializers, viewsets
from rest_framework.response import Response

from utilities.general_utils import unpack_request_body
from .models import Restaurant, MenuItem
from .utilities import update_restaurant_menu


def temporary_serializer(obj, fields):
    data = {}
    for field in fields:
        data[field] = getattr(obj, field)
    return data

#------------------------------------------------------------------------------------

class RestaurantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Restaurant
        fields = '__all__'
        # fields = ['id', 'name', 'phone', 'address1', 'address2', 'city', 'state_code', 'zip_code', 'owner']


class RestaurantViewSet(viewsets.ModelViewSet):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class MenuItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = MenuItem
        fields = ('id', 'name', 'price')


class RestaurantMenuSerializer(serializers.ModelSerializer):
    menu_items = MenuItemSerializer(many=True)

    class Meta:
        model = Restaurant
        fields = ('menu_items',)


class RestaurantMenuViewSet(viewsets.ViewSet):
    # TODO: this style wasnt working:
    # https://stackoverflow.com/questions/14573102/how-do-i-include-related-model-fields-using-django-rest-framework
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantMenuSerializer

    def retrieve(self, request, pk=None):
        queryset = Restaurant.objects.all()
        restaurant = get_object_or_404(queryset, pk=pk)
        serializer = RestaurantMenuSerializer(restaurant)
        return Response(serializer.data)

#------------------------------------------------------------------------------------

def find_restaurant_owned_by_a_username(username):
    try:
        user = User.objects.get(username=username)
    except:
        return None
    restaurants_owned = user.restaurants.all()
    if restaurants_owned:
        return restaurants_owned[0]
    else:
        return None


def restaurants_owned_by_this_username(request):
    """ for now only returns one restaurant """
    # TODO: will need to support multiple restaurants owned by one user in the future
    if request.method == "POST":
        body = unpack_request_body(request)
        username = body['username']
        restaurant_owned = find_restaurant_owned_by_a_username(username)
        if restaurant_owned:
            # TODO: getting an error here:
            #  https://stackoverflow.com/questions/47490447/django-core-exceptions-improperlyconfigured-could-not-resolve-url-for-hyperlink
            # serializer =  RestaurantSerializer(restaurant_owned, context={'request': request})
            # return JsonResponse({'result': serializer.data})
            fields = ['id', 'name', 'phone', 'address1', 'address2', 'city', 'state_code', 'zip_code']
            data = temporary_serializer(restaurant_owned, fields)
            return JsonResponse({'result': data})
        else:
            return JsonResponse({'result': None})
    else:
        error_message = "This method only responds to POST"
        print(error_message)
        return JsonResponse({'error': error_message})


def update_menu(request):
    body = unpack_request_body(request)
    update_restaurant_menu(body['menu'], body['restaurantId'])
    return JsonResponse({'result': 'seems to work so far'})
