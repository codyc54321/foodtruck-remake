//...........Load modules used in the app.......

var express = require('express');//get the express framework
var app = express();// app becomes an instance of an express server
var morgan = require('morgan');//morgan is the express middleware logger
var bodyParser = require('body-parser');// for parsing json requset
var path = require('path');// resloves paths in node
var fs = require('fs');
var config = require('./config-map');

//Webpack Modules For Development
var webpack_config = require('./webpack.config.dev.js');
var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var compiler = webpack(webpack_config);

const port = process.env.FOODTRUCK_PORT || 8080;

app.use(morgan('dev'));
app.use(bodyParser.json());

app.use(webpackDevMiddleware(compiler, {noInfo: true, publicPath: webpack_config.output.publicPath}));
app.use(webpackHotMiddleware(compiler));

app.use(express.static('assets'));

//.........Api to return app config......
app.get('/api/config', function(req, res) {
  res.send(config);
});

// A frontend app should not have to worry about also running an API
// require("./routes/api-routes.js")(app);
// require("./routes/api-restless.js")(app);

app.get('/*', function(req, res) {
    res.sendFile(path.resolve('assets/index.html'));
});

app.listen(port, function(err) {
  if(err){
      console.log('Unable to start server:');
      console.log(err);
  }
  else {
      console.log('Server listening on port ', port);
  }
});
