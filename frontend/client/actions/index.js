import { sleep } from "../utils/general_utils";


export function loginAction(email) {

    return function(dispatch) {
      dispatch({
          type: 'LOG_IN',
          payload: email
      }, sleep.bind(null, 2))
    }
}
