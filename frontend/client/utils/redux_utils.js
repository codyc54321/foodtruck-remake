
export function unpackStore(redux_store, namespace) {
    // namespace is determined by what name you gave each reducer in combineReducers; client/reducers/index.js
    let final_props = {};
    let KEYS = Object.keys(redux_store[namespace]);
    for (let key of KEYS) {
        final_props[key] = redux_store[namespace][key];
    }
    return final_props;
}

export function basicUnpackStoreClosure(namespace) {
    return function(store) {
        let props = unpackStore(store, namespace);
        return props;
    }
}
