
export function sleep(seconds) {
    var waitUntil = new Date().getTime() + seconds*1000;
    console.log('\n\nnow sleeping');
    while(new Date().getTime() < waitUntil) true;
}
