
export function checkIfAllFieldsAreEmpty(obj) {
    let keys = Object.keys(obj);
    for (let key of keys) {
        if (obj[key].length > 0) {
            return false;
        }
    }
    return true;
}


export function createNewListItemsState(oldItemsList, key, index, newInputValue, fieldNames) {
    // key is the field name, like 'price'
    // index is the index of what was changed

    let newItemsList = oldItemsList.slice(0);

    // check if you have to remove a newly empty object
    newItemsList[index][key] = newInputValue;
    if (checkIfAllFieldsAreEmpty(newItemsList[index])) {
        newItemsList.splice(index, 1);
    }

    // check if you have to add a new empty object to the end
    let last_item = newItemsList[newItemsList.length - 1];
    if (!checkIfAllFieldsAreEmpty(last_item)) {
        let newObject = {};
        for (let key of fieldNames) {
            newObject[key] = '';
        }
        newItemsList.push(newObject);
    }

    return newItemsList;
}


export function deleteFromItemList(oldItemsList, indexToDelete) {
    let newItemsList = oldItemsList.slice(0);
    let anItem = oldItemsList[0];
    let fieldNames = Object.keys(anItem);
    let isLastItem = (newItemsList.length - 1) == indexToDelete;
    if (isLastItem) {
        for (let field of fieldNames) {
            newItemsList[indexToDelete][field] = '';
        }
    } else {
        newItemsList.splice(indexToDelete, 1);
    }
    return newItemsList;
}

// specific to this app ----------------------------------------

export function isPriceChangeValid(newPrice) {
    let re = /^\d+[.]?\d{0,2}$/;
    return re.test(newPrice);
}


export function determineIfRunForPrice(newValue) {
    if (isPriceChangeValid(newValue) || newValue.length == 0) {
        return true;
    }
    return false;
}
