import axios from "axios";

const BASE_API_URL = "http://127.0.0.1:8000"
const RESTAURANTS_OWNED_URL = `${BASE_API_URL}/api/restaurants-owned/`
const GET_USER_ID_URL = `${BASE_API_URL}/api/user-id/`;
const GET_MENU_URL = `${BASE_API_URL}/api/menus/`;


export function getUserIdFromUsername(username, dispatch) {
    axios.post(GET_USER_ID_URL, {username})
    .then(function (response) {
        if (response.data.result) {
            dispatch({type: 'SET_USER_ID', payload: response.data.result});
        }
    })
    .catch(function (error) {
        alert(error);
    });
}


export function getRestaurantInfoForUser(username, dispatch) {
    axios.post(RESTAURANTS_OWNED_URL, {username})
    .then(function(response) {
        console.log('\n\n\nOH YA got it baby: ');
        console.log(response.data.result);
        if (response.data.result){
            dispatch({type: 'SET_RESTAURANT_INFO', payload: response.data.result});
            let getThisMenuUrl = GET_MENU_URL + response.data.result.id;
            alert(getThisMenuUrl);
            axios.get(getThisMenuUrl)
            .then(function(response) {
                alert('got a response on menu')
                console.log(response);
                let menuItems = response.data.menu_items.slice();
                menuItems.push({name: '', price: ''});
                dispatch({type: 'SET_MENU_ITEMS', payload: menuItems})
            })
            .catch(function (error) {
                alert(error);
            });
        }
    })
    .catch(function (error) {
        alert(error);
    });
}
