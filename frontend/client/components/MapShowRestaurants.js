import React from "react";
import { browserHistory } from "react-router";
import axios from "axios";
import ReactLoading from "react-loading";

import MyComponent from "./MyComponent";
import MapView from "./maps/MapView";
import ProfileViewRestaurantBasic from "./ProfileViewRestaurantBasic";
import geocode from "../api_clients/map";

const RESTAURANTS_DATA_URL = "http://127.0.0.1:8000/api/restaurants/"


export default class MapShowRestaurants extends MyComponent {

    constructor(props){
        let custom_methods = [
            "renderBodyHTML",
            "clickMarkerCustomers",
        ]
        super(props, custom_methods);
        this.state = {
            restaurantName: null,
            markers_data: [],
            fetch_restaurants_error: false,
            fetch_restaurants_complete: false
        }
    }

    componentDidMount() {
        axios.get(RESTAURANTS_DATA_URL)
          .then((results) => {
              console.log('did we get to end of the axios.get???');
              this.setState({markers_data: results.data, fetch_restaurants_complete: true});
          })
          .catch((error) => {
              console.log(error);
              alert('error')
              this.setState({fetch_restaurants_error: true});
          });
    }

    render() {
        let titleText = "Order Food";
        let description = "";

        return (
            <div>
                <h3>{titleText}</h3>
                <p>{description}</p>
                {this.renderBodyHTML()}
            </div>
        );

        // <ProfileViewRestaurantBasic restaurantID={this.state.restaurantID} />
    }

    renderBodyHTML() {
        let self = this;
        let body;

        if (this.state.fetch_restaurants_error) {
            body = (<h4>Error fetching restaurants data</h4>);
        } else if (!this.state.fetch_restaurants_complete) {
            body = (<ReactLoading type="cylon" color="yellow" height='667' width='375' />);
        } else {
            console.log('those markers are');
            console.log(this.state.markers_data);

            let markers_data = this.state.markers_data.map(function(dataset) {
                let coordinates = {lat: parseFloat(dataset.lat), lng: parseFloat(dataset.lng)};
                console.log(coordinates);
                return {
                    callback: self.clickMarkerCustomers,
                    params: [dataset.name],
                    coordinates: coordinates
                }
            })

            let warning_message = (<div></div>);
            if (this.state.markers_data.length < 1) {
                let message = "Sorry, but we don't have any active restaurants";
                warning_message = (<p>{message}</p>);
            }

            body = (
                <div>
                    {warning_message}
                    <MapView
                        markers_data={markers_data}
                        center={{lat: 30.4753596, lng: -98.1558200}} />
                    {this.state.restaurantName}
                </div>
            )
        }

        return body;
    }

    clickMarkerCustomers(name) {
        this.setState({ restaurantName: name});
    }
}
