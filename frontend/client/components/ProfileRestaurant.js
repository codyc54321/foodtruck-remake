import React, {Component} from "react";


export default class ProfileRestaurant extends Component {

    // now being used right now
    // but restaurants should have a full profile view,
    // which gives pictures and more info than the abbreviated profile on the map
    render() {
        return (
            <div>
                <p>This is the profile of restaurant with ID == {this.props.location.query.item_id}</p>
            </div>
        );
    }
}
