// TESTING GOOGLE MAPS GEOCODING API - not sure where to implement this code, might use AJAX call instead
var maps = require('@google/maps').createClient({
  key: 'AIzaSyBQF8ObbvuQLJDt-en7VICr5VEX18C13VU'
});

// Geocode an address.
var geocode = {
    getCoord: function(defaultAddress, restaurantName) {
        // expects something like:
        //    "3415 Appendale Dr Austin TX 78755"
        return new Promise(function(resolve, reject){
            maps.geocode({
                address: defaultAddress
            }, function(err, response) {
                if (!err) {
                    let result = response.json.results[0].geometry.location;
                    console.log(result);
                    console.log("resolving...");
                    resolve({
                        restaurantName: restaurantName,
                        geocodes: result
                    });
                    console.log("resolved");
                    //array.push({restaurantName: restaurantName, geocodes: result});
                    //finished[restaurantName] = true;
                } else {
                    console.log("error in promise");
                    console.log(err);
                    reject(0);
                }
            });
        });
    },

    getAddr: function(){
        maps.reverseGeocode({
            latlng: coordinates
        }, function(err, response) {
            if (!err) {
                console.log(response.json.results);
            }
        });
    }
}

// Reverse geocode - turn dropped pin w/ latitude/longitude into address

module.exports = geocode;
