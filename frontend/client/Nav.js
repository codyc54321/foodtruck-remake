import React from 'react';
import { Link } from 'react-router';
import { connect } from "react-redux"

import { basicUnpackStoreClosure } from "./utils/redux_utils";
import MyComponent from "./components/MyComponent";


@connect(basicUnpackStoreClosure('user_info'))
export default class Nav extends MyComponent {
    constructor(props) {
        let custom_methods = [
            'signOut',
        ]
        super(props, custom_methods);
    }

    signOut(event) {
        // event.preventDefault();
        this.props.dispatch({type: 'LOG_IN', payload: null})
    }

    render() {

        let email_box = null;
        let email = this.props.email;

        if (email) {
            email_box = (
                <div>
                    {email}
                </div>
            );
        }

        let userLink = (<li><Link activeClassName="active" to="/login">Login/Register</Link></li>);
        let registerRestaurantLink;
        let editMenuLink;
        let welcome;
        if (this.props.email) {

            userLink = (
                <li><a onClick={() => { this.signOut() }}>Sign Out</a></li>
            );

            registerRestaurantLink = (
                <li><Link activeClassName="active" to="/registerRestaurant">Signup your Restaurant</Link></li>
            );

            editMenuLink = (
                <li><Link activeClassName="active" to="/editMenu">Edit your menu</Link></li>
            );

            welcome = (
                <div id="welcome" className="card">
                    Welcome back, {email_box}
                </div>
            );
        }

        return (
            <div className="container">
                <nav>
                    <div className="nav-wrapper">
                        <a className="brand-logo" href="/">Nyble</a>
                        <ul id="nav-mobile" className="right hide-on-med-and-down">
                            <li><Link activeClassName="active" to="/" onlyActiveOnIndex>Home</Link></li>
                            {registerRestaurantLink}
                            {editMenuLink}
                            {userLink}
                        </ul>
                    </div>
                </nav>

                {welcome}

                <div className="container">
                    {this.props.children}
                </div>
                <br/><br/>
            </div>
        )
    }
}
