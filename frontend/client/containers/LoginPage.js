import React from "react";
import axios from "axios";
import { connect } from "react-redux"
import { browserHistory } from "react-router";

import MyComponent from "../components/MyComponent";
import MapShowRestaurants from "../components/MapShowRestaurants";
import { basicUnpackStoreClosure } from "../utils/redux_utils";
import { loginAction } from "../actions/index";
import { getUserIdFromUsername, getRestaurantInfoForUser } from "../utils/user_info_utils";

const BASE_API_URL = "http://127.0.0.1:8000"
const REGISTER_USER_URL = `${BASE_API_URL}/api/register-user/`
const AUTHENTICATE_URL = `${BASE_API_URL}/api/authenticate/`


@connect(basicUnpackStoreClosure('user_info'))
export default class LoginPage extends MyComponent {

    constructor(props) {
        let custom_methods = [
            'handleLoginOrRegisterToggle',
            'handleOnKeyDownInInputs',
            'onLoginSubmit',
            'fakeInstantOnLoginSubmit',
            'onRegisterSubmit',
            'getRestaurantInfoOnLoginOrRegister',
        ];
        super(props, custom_methods);
        this.state = {
            mode: 'login',
            email: 'cchilders',
            password: 'password',
            password_confirm: ''
        };
        if (props.mode == 'register') {
            this.state.mode = 'register';
        }
    }

    render() {
        let loginStyle = null;
        let registerStyle = null;
        let form = null;

        let activeStyle = {backgroundColor: '#ee6e73', color: 'white'};

        if (this.state.mode == 'login') {
            loginStyle = activeStyle;

            form = (
                <div className="input-field">
                    <input
                        placeholder="email"
                        type="text"
                        value={this.state.email}
                        onChange={(event) => {this.handleInputchange(event, 'email')}}
                        onKeyDown={(event) => { this.handleOnKeyDownInInputs(event, 'login') }}
                         />
                    <br/>
                    <input
                        placeholder="password"
                        type="password"
                        value={this.state.password}
                        onChange={(event) => {this.handleInputchange(event, 'password')}}
                        onKeyDown={(event) => { this.handleOnKeyDownInInputs(event, 'login') }}
                         />
                    <br/>
                    <button
                        className="waves-effect waves-light btn red lighten-2"
                        type="submit"
                        onClick={this.onLoginSubmit}
                    >
                        Submit
                    </button>
                    <br/><br/>
                </div>
            );

        } else if (this.state.mode == 'register') {
            registerStyle = activeStyle;

            form = (
                <div className="input-field">
                    <input
                        placeholder="email"
                        type="text"
                        value={this.state.email}
                        onChange={(event) => {this.handleInputchange(event, 'email')}}
                        onKeyDown={(event) => { this.handleOnKeyDownInInputs(event, 'register') }}/> <br/>
                    <input
                        placeholder="password"
                        type="password"
                        value={this.state.password}
                        onChange={(event) => {this.handleInputchange(event, 'password')}}
                        onKeyDown={(event) => { this.handleOnKeyDownInInputs(event, 'register') }} /> <br/>
                    <input
                        placeholder="confirm password"
                        type="password"
                        value={this.state.password_confirm}
                        onChange={(event) => {this.handleInputchange(event, 'password_confirm')}}
                        onKeyDown={(event) => { this.handleOnKeyDownInInputs(event, 'register') }} /> <br/>

                    <button
                        className="waves-effect waves-light btn red lighten-2"
                        type="submit"
                        onClick={this.onRegisterSubmit}
                    >
                        Submit
                    </button>
                    <br/><br/>
                </div>
            );
        }

        return (
            <div>
                <br/><br/>
                <button
                    className="waves-effect waves-light btn"
                    onClick={() => { this.handleLoginOrRegisterToggle('login') } }
                    style={loginStyle}>Login</button>

                <button
                    className="waves-effect waves-light btn"
                    onClick={() => { this.handleLoginOrRegisterToggle('register') } }
                    style={registerStyle}>Register</button>

                <br/>
                {form}
            </div>
        )
    }

    handleLoginOrRegisterToggle(mode) {
        this.setState({mode: mode});
        // https://stackoverflow.com/questions/28889826/react-set-focus-on-input-after-render
        this.emailField.focus();

    }

    handleOnKeyDownInInputs(event, mode) {
        if (event.keyCode == 13) {
            if (mode == 'login') {
                this.onLoginSubmit();
            } else if (mode == 'register') {
                this.onRegisterSubmit();
            } else {
                throw "whoops"
            }
        }
    }

    onLoginSubmit() {
        let self = this;
        let email = self.state.email;
        axios.post(AUTHENTICATE_URL, {
            username: email,
            password: this.state.password
        })
        .then(function (response) {
            if (response.data.result) {
                console.log(`user ${email} logged in`);
                self.props.dispatch({type: 'LOG_IN', payload: email});
                self.getRestaurantInfoOnLoginOrRegister(email);
                browserHistory.push(self.props.destination_url);
            } else {
                alert("Serious logic error in LoginPage.js");
            }
        })
        .catch(function (error) {
            console.log("Something went wrong trying to log in");
            console.log(error);
            alert("Sorry, that username/password combination didn't work");
        });
    }

    fakeInstantOnLoginSubmit() {
        let self = this;
        let reroute = function() {
            browserHistory.push(self.props.destination_url);
        }
        this.props.dispatch({type: 'LOG_IN', payload: "fake@fake.com"})
          .then((response) => {
              browserHistory.push(self.props.destination_url);
          })
        reroute();
    }

    onRegisterSubmit() {
        let self = this;
        let email = this.state.email;
        let password = this.state.password;
        let password_confirmation = this.state.password_confirm;

        if (!email) {
            alert("enter an email");
            return;
        }
        if (!password) {
            alert("enter a password");
            return;
        }
        if (!password_confirmation) {
            alert("confirm your password");
            return;
        }
        if (!(password == password_confirmation)) {
            alert("Passwords must match");
            return;
        }

        axios.post(REGISTER_USER_URL, {
            username: email,
            password
        })
        .then(function (response) {
            if (response.result == "USER_BUT_NO_PW_MATCH") {
                alert("Sorry, a user account with that email already exists");
            } else {
                console.log(response);
                console.log(`user ${email} already had an account`);
                self.props.dispatch({type: 'LOG_IN', payload: email});
                self.getRestaurantInfoOnLoginOrRegister(email);
                browserHistory.push(self.props.destination_url);
            }
        })
        .catch(function (error) {
            alert(error);
        });
    }

    getRestaurantInfoOnLoginOrRegister(username) {
        let this_dispatch = this.props.dispatch;
        getUserIdFromUsername(username, this_dispatch);
        getRestaurantInfoForUser(username, this_dispatch);
    }
}


LoginPage.defaultProps = {
    destination_url: '/',
}
