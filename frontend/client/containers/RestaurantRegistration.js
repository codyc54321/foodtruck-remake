import React from 'react';
import axios from "axios";
import { connect } from "react-redux"
import { browserHistory } from "react-router";

import MyComponent from '../components/MyComponent';
import { basicUnpackStoreClosure } from "../utils/redux_utils";


let RESTAURANT_INFO_FIELDS = [
    'id',
    'name',
    'address1',
    // 'address2',
    'city',
    'state_code',
    'zip_code',
    'phone'
];


@connect(basicUnpackStoreClosure('user_info'))
export default class RestaurantRegistration extends MyComponent {
    constructor(props) {
        let custom_methods = [
            'unpackRestaurantInfoToState',
            'onTruckSubmit',
        ]
        super(props, custom_methods);
        this.state = {
            id: null,
            name: '',
            address1: '',
            city: '',
            state_code: '',
            zip_code: '',
            phone: '',

            restaurant_existed: null,
        };
    }

    componentDidMount() {
        // check if restaurant_info exists or not
        console.log("this.props.restaurant_info in componentDidMount:", this.props.restaurant_info);
        if (this.props.restaurant_info) {
            this.unpackRestaurantInfoToState(this.props.restaurant_info);
        } else {
            // make that API call
        }
    }

    unpackRestaurantInfoToState(input_data) {
        let new_state = {};
        console.log("this.props.restaurant_info in unpackRestaurantInfoToState():", this.props.restaurant_info)
        for (let field of RESTAURANT_INFO_FIELDS) {
            new_state[field] = input_data[field];
        }
        console.log('setting new state in unpackRestaurantInfoToState: ', new_state);
        this.setState(new_state);
    }

    render() {
        console.log("state before render:")
        console.log(this.state);
        return (
            <div className="input-field">
                <form>
                    <input placeholder="Restaurant Name" type="text" size="43" value={this.state.name} onChange={(event) => {this.handleInputchange(event, 'name')}} /><br/><br/>
                    <input placeholder="Street Address" type="text" size="43" value={this.state.address1} onChange={(event) => {this.handleInputchange(event, 'address1')}} /><br/><br/>
                    <input placeholder="City" type="text" size="20" value={this.state.city} onChange={(event) => {this.handleInputchange(event, 'city')}} />
                    <input placeholder="State" type="text" maxLength="2" size="10" value={this.state.state_code} onChange={(event) => {this.handleInputchange(event, 'state_code')}} />
                    <input placeholder="ZIP Code" type="number" maxLength="5" size="10" value={this.state.zip_code} onChange={(event) => {this.handleInputchange(event, 'zip_code')}} /><br/><br/>
                    <input placeholder="Phone Number" type="number" maxLength="10" size="43" value={this.state.phone} onChange={(event) => {this.handleInputchange(event, 'phone')}} /><br/><br/>
                    <input className="waves-effect waves-light btn red lighten-2" type="submit" onClick={this.onTruckSubmit} />
                </form>
            </div>
        )
    }

    onTruckSubmit(event){
        event.preventDefault();

        let RESTAURANT_URL = "http://127.0.0.1:8000/api/restaurants/";
        let GET_USER_ID_URL = "http://127.0.0.1:8000/api/user-id/";

        let self = this;

        let body = {};
        for (let field of RESTAURANT_INFO_FIELDS) {
            body[field] = this.state[field];
        }

        if (this.state.id) {
            // alert('there is a restaurant ID already')
            let RESTAURANT_UPDATE_URL = `${RESTAURANT_URL}${this.state.id}/`;
            axios.put(RESTAURANT_UPDATE_URL, body)
              .then(function(response){
                  console.log("did a PUT")
                  console.log(response);
                  self.unpackRestaurantInfoToState(response.data);
                  self.props.dispatch({type: 'SET_RESTAURANT_INFO', payload: response.data});
              })
              .catch(function(error) {
                  console.log("error after PUT");
                  console.log(error)
              })
        } else {
            // else, this user does not have a restaurant on file yet
            body.owner = this.props.user_id;
            axios.post(RESTAURANT_URL, body)
            .then(function(response){
                console.log("did a POST")
                console.log(response);
                self.unpackRestaurantInfoToState(response.data);
                self.props.dispatch({type: 'SET_RESTAURANT_INFO', payload: response.data});
            })
            .catch(function(error) {
                console.log("error after POST");
                console.log(error)
            })
        }
    }
}
