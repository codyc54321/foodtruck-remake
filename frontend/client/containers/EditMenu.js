import React from "react";
// import axios from "axios";
import { connect } from "react-redux"

import MyComponent from "../components/MyComponent";
import { basicUnpackStoreClosure } from "../utils/redux_utils";
import {
    checkIfAllFieldsAreEmpty,
    createNewListItemsState,
    deleteFromItemList,
    isPriceChangeValid,
    determineIfRunForPrice
} from "../utils/list_item_updates_utils";


// @connect((store) => {
//     return {
//         menu_items: store.menu_items_store.menu_items,
//     }
// })
@connect(basicUnpackStoreClosure("user_info"))
export default class EditMenu extends MyComponent {
    constructor(props) {
        let custom_methods = [
            'handleInputChange',
            'deleteEntry',
            'focusInputText',
        ]
        super(props, custom_methods);
    }

    componentDidMount() {}

    render() {
        // console.log("state before render:")
        // console.log(this.state)

        let self = this;

        return (
            <div className="input-field">
                <form>
                    {self.renderInputs()}
                    <input className="waves-effect waves-light btn red lighten-2" type="submit" onClick={this.onTruckSubmit} />
                </form>
            </div>
        )
    }

    renderInputs() {
        console.log('\n\n\nso what menu items did we have?');
        console.log(this.props.menu_items);
        return this.props.menu_items.map((menu_item_dataset, index) => {
            let clear_button = null;
            let { name, price } = menu_item_dataset;

            if (name || price) {
                clear_button = (<button className="closeButton" onClick={(event) => {this.deleteEntry(event, index)}}>DELETE</button>);
            }

            return (
                <div key={index}>
                    <input
                        type="text"
                        value={name}
                        onFocus={(event) => {this.focusInputText(event)}}
                        onClick={(event) => {this.focusInputText(event)}}
                        onChange={(event) => {this.handleInputChange(event, 'name', index)}}
                        placeholder="name" />
                    <input
                        type="text"
                        value={price}
                        onFocus={(event) => {this.focusInputText(event)}}
                        onClick={(event) => {this.focusInputText(event)}}
                        onChange={(event) => {this.handleInputChange(event, 'price', index)}}
                        placeholder="price"
                        className="priceInput" />
                    {clear_button}
                </div>
            );
        })
    }

    handleInputChange(event, key, index) {
        let runUpdate = true;
        let filterMap = {price: determineIfRunForPrice};
        if (filterMap[key]) {
            runUpdate = filterMap[key](event.target.value);
        }

        if (runUpdate) {
            let newState = createNewListItemsState(
                this.props.menu_items,
                key,
                index,
                event.target.value,
                ['name', 'price']
            );
            this.props.dispatch({type: 'SET_MENU_ITEMS', payload: newState});
        }
    }

    deleteEntry(event, index) {
        event.preventDefault();
        let newMenuItems = deleteFromItemList(this.props.menu_items, index);
        this.props.dispatch({type: 'SET_MENU_ITEMS', payload: newMenuItems});
    }

    focusInputText(event) {
        event.target.select();
    }


}
