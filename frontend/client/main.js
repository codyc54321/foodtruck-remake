import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { Router, Route, Link, IndexRoute, browserHistory } from "react-router";

import reducers from "./reducers";
import { loadConfig, getConfig } from "./config";
import Nav from "./Nav";
import LoginPage from "./containers/LoginPage";
import MapShowRestaurants from "./components/MapShowRestaurants";
import RestaurantRegistration from "./containers/RestaurantRegistration";
import EditMenu from "./containers/EditMenu";

const createStoreWithMiddleware = applyMiddleware()(createStore);


getConfig.then((config) => {

    loadConfig(config);

    ReactDOM.render(
        (
            <Provider store={createStoreWithMiddleware(reducers)}>
                <Router history={browserHistory}>
                    <Route path="/" component={Nav}>
                        <IndexRoute component={MapShowRestaurants} />
                        <Route path="/login" component={LoginPage} />
                        <Route path="/registerRestaurant" component={RestaurantRegistration} />
                        <Route path="/editMenu" component={EditMenu} />
                    </Route>
                </Router>
            </Provider>
        ), document.querySelector('.container'));
})
.catch((err) => {
    console.log(err);
})
