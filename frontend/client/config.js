import 'whatwg-fetch'; //imports as fetch
//these objects get populated from the server. To add values to your config
//add them to the coresponding object in client-map.js file in the root of the
//repo. To add new groups of values follow the pattern below. Export the named
//object here, then create that object with mapping in client-map.js file.
//Finally, populate the object in the loadConfig function below using the same
//the populate function
// export const base_urls = {};
// export const metricsPortalAuthCredentials = {};
// export const roleCheckerAuthCredentials = {};
// export const nav_links = {};
// export const user_roles = {};

//must be called on web app load request config vars from server
export let getConfig = fetch('/api/config')
  .then((res) => {
    if (res.error) throw(res.error);
    return res.json();
  });

// call this from the then function of getConfig, then render the page
// all config exports should work at this point
export function loadConfig (config) {
    // let processed_nav_links = config.nav_links;
    // let order_metrics_links_data = config.order_metrics_type_map.map(
    //     function(dataset){
    //         return {type: 'react_link', to: { pathname: '/ordermetrics', query: {orderType: dataset.slug }}, text: dataset.display_name};
    //     }
    // );
    // processed_nav_links['order_metrics_links_data'] = order_metrics_links_data;
    //
    // populate(base_urls, config.base_urls);
    // populate(metricsPortalAuthCredentials, config.metricsPortalAuthCredentials);
    // populate(roleCheckerAuthCredentials, config.roleCheckerAuthCredentials);
    // populate(nav_links, processed_nav_links);
    // populate(user_roles, config.user_roles);
    // populate(email_keys, config.email_keys);
}

function populate (objToPop, fromObj) {
   for (let key in fromObj) {
    objToPop[key] = fromObj[key];
  }
}
