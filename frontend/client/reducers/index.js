import { combineReducers } from 'redux';


const USER_INFO_DEFAULTS = {
    email: null,
    user_id: null,
    restaurant_info: null,
    menu_items: [
        {name: '', price: ''},
        // {name: 'steak taco', price: '2.00'},
        // {name: 'chicken burrito', price: '6.50'}
    ],
}


function userInfoReducer(state=USER_INFO_DEFAULTS, action) {
    switch (action.type) {
      case 'LOG_IN': {
        return {...state, email: action.payload}
      };
      case 'SET_USER_ID': {
        return {...state, user_id: action.payload}
      }
      case 'SET_RESTAURANT_INFO': {
        return {...state, restaurant_info: action.payload}
      }
      case 'SET_MENU_ITEMS': {
        return {...state, menu_items: action.payload}
      }
    }
    return state
}


let reducers = {
    user_info: userInfoReducer,
};

export default combineReducers(reducers);

//
// const rootReducer = combineReducers({
//   state: (state = {}) => state
// });
//
// export default rootReducer;
