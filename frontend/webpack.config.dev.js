var webpack = require('webpack');

module.exports = {
  //devtool: 'inline-source-map',
  entry: [
    'webpack-hot-middleware/client',
    './client/main.js'
  ],
  output: {
    path: require("path").resolve('./assets'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  externals: {
      jquery: 'var jQuery'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['react', 'es2015', 'react-hmre'],
          plugins: ['transform-decorators-legacy']
        }
      },
      {
          include: /\.json$/,
          loader: 'json-loader'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader',
        exclude: /node_modules/
      }
    ]
  }
};
