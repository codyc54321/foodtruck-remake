"use strict";

var should = require("chai").should();
// var isPriceChangeValid = require('../client/utils/string_utils').isPriceChangeValid;
var isPriceChangeValid = require('../client/utils/list_item_updates_utils').isPriceChangeValid;
var checkIfAllFieldsAreEmpty = require('../client/utils/list_item_updates_utils').checkIfAllFieldsAreEmpty;


describe("isPriceChangeValid()", function() {
  it("should allow 2 decimals", function() {
    isPriceChangeValid("245.54").should.equal(true);
  });

  it("should also 1 decimal (because we can just convert it)", function() {
    isPriceChangeValid("13.4").should.equal(true);
  });

  it("should allow no decimals", function() {
    isPriceChangeValid("873").should.equal(true);
  });

  it("should allow a dot with no decimals", function() {
    isPriceChangeValid("88.").should.equal(true);
  });

  it("should not allow longer than 2 decimals", function() {
    isPriceChangeValid("214.983").should.equal(false);
  });

  it("should not allow letters", function() {
    isPriceChangeValid("234h.23").should.equal(false);
  });

  it("should not allow characters like & ^ $ %", function() {
    isPriceChangeValid("234.23$").should.equal(false);
  });
});


describe("checkIfAllFieldsAreEmpty()", function() {
  it("should return true if all are empty", function() {
    checkIfAllFieldsAreEmpty({name: '', price: ''}).should.equal(true);
  });

  it("should return false if some aren't empty", function() {
    checkIfAllFieldsAreEmpty({name: 'Whataburger', price: ''}).should.equal(false);
  });
});
